# CSV

A class for manipulating CSV files using a simple, fluent interface.

## Creating

Instantiate with a path to a CSV, a multidimensional array or nothing:

```
$csv = new CSV( '/path/to/csv.csv' );

$csv = new CSV([
    ['col_1','col_2'],
    ['1','2'],
    ['3','4'],
]);

$csv = new CSV;
```

A static make method allows fluent chaining of methods:

```
$csv = CSV::make( '/path/to/csv.csv' )
    ->append([
        ['1','2'],
        ['3','4'],
    ])
;
```

## Output




