<?php
Namespace dgifford\CSV\Tests;



use dgifford\CSV\CSV;



require __DIR__ . '/../vendor/autoload.php';


class InstantiationTest extends TestCase
{
	public function testWithoutArguments()
	{
        $csv = new CSV;

		$this->assertInstanceOf( 'dgifford\CSV\CSV', $csv );

        $csv = CSV::make();

		$this->assertInstanceOf( 'dgifford\CSV\CSV', $csv );
	}



	public function testWithPath()
	{
        $csv = new CSV( $this->csv_1_path );

        $this->assertInstanceOf( 'dgifford\CSV\CSV', $csv );

        $this->assertSame( 3, $csv->getRowCount() );

        $this->assertSame( $this->csv_1_array, $csv->toArray() );

        $this->assertSame( "col_1,col_2\r\n1,2\r\n3,4", $csv->toString() );
    }



	public function testWithCustomDelimitedFile()
	{
        $csv = new CSV( $this->custom_delimiter_file_path );

        $this->assertSame( 3, $csv->getRowCount() );

        // Delimiter has not been replaced
        $this->assertSame( ['col_1;col_2'], $csv[0] );
    }



	public function testWithCustomDelimiterSet()
	{
        $csv = CSV::make()
            ->setDelimiter(';')
            ->replace($this->custom_delimiter_file_path )
        ;

        $this->assertSame( 3, $csv->getRowCount() );

        $this->assertSame( ['col_1','col_2'], $csv[0] );
    }



	public function testWithInvalidPath()
	{
        $this->expectException( \InvalidArgumentException::class );

        $csv = new CSV( '/foo/bar.csv' );
    }



	public function testWithInvalidFile()
	{
        $this->expectException( \InvalidArgumentException::class );

        $csv = new CSV( $this->invalid_file_path );
    }



    public function testWithArray()
    {
        $csv = new CSV([
            ['col_1','col_2'],
            ['1','2'],
            ['3','4'],
        ]);

        $this->assertSame( 3, $csv->getRowCount() );

        $this->assertSame( 2, $csv->getColumnCount() );
    }



    public function testWithEmptyArray()
    {
        $this->expectException( \InvalidArgumentException::class );

        $csv = new CSV([]);
    }



    public function testWithFileWithTrailingLineEnding()
    {
        $csv = CSV::make( $this->csv_2_path );

        $this->assertInstanceOf( 'dgifford\CSV\CSV', $csv );

        $this->assertSame( 3, $csv->getRowCount() );

        $this->assertSame( $this->csv_2_array, $csv->toArray() );
    }

}