<?php
Namespace dgifford\CSV\Tests;



use dgifford\CSV\CSV;



require __DIR__ . '/../vendor/autoload.php';



class TestCase extends \PHPUnit\Framework\TestCase
{
	public function setUp():void
	{
		$this->csv_1_path = __DIR__ . '/data/csv_1.csv';

		$this->csv_2_path = __DIR__ . '/data/csv_2.csv';

		$this->csv_3_path = __DIR__ . '/data/csv_3.csv';

        $this->csv_1_array = [
            ['col_1','col_2'],
            ['1','2'],
            ['3','4'],
        ];

        $this->csv_2_array = [
            ['col_1','col_2'],
            ['5','6'],
            ['7','8'],
        ];

        $this->csv_3_array = [
            ['col_1','col_2'],
            ['9','10'],
            ['11','12'],
        ];

        $this->special_values_csv_path = __DIR__ . '/data/special_values.csv';

		$this->temp_csv_path = __DIR__ . '/_output/temp.csv';

        $this->invalid_file_path = __DIR__ . '/data/invalid_file.json';

        $this->custom_delimiter_file_path = __DIR__ . '/data/custom_delimiter.csv';
	}


    public function deleteFile( string $path )
    {
        if( is_file($path))
        {
            unlink($path);
        }
    }
}