<?php
Namespace dgifford\CSV\Tests;



use dgifford\CSV\CSV;



require __DIR__ . '/../vendor/autoload.php';



class SpliceTest extends TestCase
{
    public function setUp():void
    {
        parent::setUp();

        $this->csv_1 = new CSV( $this->csv_1_path );

        $this->csv_2 = new CSV( $this->csv_2_path );

        $this->inserted_result = [
            ['col_1','col_2'],
            ['col_1','col_2'],
            ['5','6'],
            ['7','8'],
            ['1','2'],
            ['3','4'],
        ];
    }



    public function testWithInvalidOffset()
    {
        $this->expectException( \InvalidArgumentException::class );

        $this->csv_1->splice( -4, 0, $this->csv_2_path );
    }



    public function testWithInvalidLength()
    {
        $this->expectException( \InvalidArgumentException::class );

        $this->csv_1->splice( 0, -1, $this->csv_2_path );
    }


    public function testPrependFile()
    {
        $this->csv_1->splice( 0, 0, $this->csv_2_path );

        $this->assertSame( array_merge( $this->csv_2_array, $this->csv_1_array ), $this->csv_1->toArray() );
    }



    public function testInsertFileInMiddle()
    {
        $this->csv_1->splice( 1, 0, $this->csv_2_path );

        $this->assertSame( $this->inserted_result, $this->csv_1->toArray() );
    }



    public function testInsertFileAtEnd()
    {
        $this->csv_1->splice( 3, 0, $this->csv_2_path );

        $this->assertSame( array_merge( $this->csv_1_array, $this->csv_2_array  ), $this->csv_1->toArray() );
    }



    public function testInsertFileBeyondEnd()
    {
        $this->csv_1->splice( 6, 0, $this->csv_2_path );

        $this->assertSame( array_merge( $this->csv_1_array, $this->csv_2_array  ), $this->csv_1->toArray() );
    }



    public function testAppendFile()
    {
        $this->csv_1->splice( -1, 0, $this->csv_2_path );

        $this->assertSame( array_merge( $this->csv_1_array, $this->csv_2_array  ), $this->csv_1->toArray() );
    }



    public function testPrependArray()
    {
        $this->csv_1->splice( 0, 0, $this->csv_2_array );

        $this->assertSame( array_merge( $this->csv_2_array, $this->csv_1_array ), $this->csv_1->toArray() );
    }



    public function testInsertArrayInMiddle()
    {
        $this->csv_1->splice( 1, 0, $this->csv_2_array );

        $this->assertSame( $this->inserted_result, $this->csv_1->toArray() );
    }



    public function testInsertArrayAtEnd()
    {
        $this->csv_1->splice( 3, 0, $this->csv_2_array );

        $this->assertSame( array_merge( $this->csv_1_array, $this->csv_2_array  ), $this->csv_1->toArray() );
    }



    public function testInsertArrayBeyondEnd()
    {
        $this->csv_1->splice( 6, 0, $this->csv_2_array );

        $this->assertSame( array_merge( $this->csv_1_array, $this->csv_2_array  ), $this->csv_1->toArray() );
    }



    public function testAppendArray()
    {
        $this->csv_1->splice( -1, 0, $this->csv_2_array );

        $this->assertSame( array_merge( $this->csv_1_array, $this->csv_2_array  ), $this->csv_1->toArray() );
    }



    public function testPrependCSV()
    {
        $this->csv_1->splice( 0, 0, $this->csv_2 );

        $this->assertSame( array_merge( $this->csv_2_array, $this->csv_1_array ), $this->csv_1->toArray() );
    }



    public function testInsertCSVInMiddle()
    {
        $this->csv_1->splice( 1, 0, $this->csv_2 );

        $this->assertSame( $this->inserted_result, $this->csv_1->toArray() );
    }



    public function testInsertCSVAtEnd()
    {
        $this->csv_1->splice( 3, 0, $this->csv_2 );

        $this->assertSame( array_merge( $this->csv_1_array, $this->csv_2_array  ), $this->csv_1->toArray() );
    }



    public function testInsertCSVBeyondEnd()
    {
        $this->csv_1->splice( 6, 0, $this->csv_2 );

        $this->assertSame( array_merge( $this->csv_1_array, $this->csv_2_array  ), $this->csv_1->toArray() );
    }



    public function testInsertCSVBeforeStart()
    {
        $this->expectException( \InvalidArgumentException::class );

        $this->csv_1->splice( -4, 0, $this->csv_2 );
    }



    public function testAppendCSV()
    {
        $this->csv_1->splice( -1, 0, $this->csv_2 );

        $this->assertSame( array_merge( $this->csv_1_array, $this->csv_2_array  ), $this->csv_1->toArray() );
    }


    public function testDeleteFirstRow()
    {
        $this->csv_1->splice( 0, 1 );

        $this->assertSame( 2, $this->csv_1->getRowCount() );

        $this->assertSame( [
            ['1','2'],
            ['3','4'],
        ], $this->csv_1->toArray() );

        $this->assertFalse( isset($this->csv_1[2]) );
    }



    public function testDeleteMiddleRow()
    {
        $this->csv_1->splice( 1, 1 );

        $this->assertSame( 2, $this->csv_1->getRowCount() );

        $this->assertSame( [
            ['col_1','col_2'],
            ['3','4'],
        ], $this->csv_1->toArray() );

        $this->assertFalse( isset($this->csv_1[2]) );
    }


    public function testDeleteLastRow()
    {
        $this->csv_1->splice( 2, 1 );

        $this->assertSame( 2, $this->csv_1->getRowCount() );

        $this->assertSame( [
            ['col_1','col_2'],
            ['1','2'],
        ], $this->csv_1->toArray() );

        $this->assertFalse( isset($this->csv_1[2]) );
    }



    public function testDeleteLastTwoRows()
    {
        $this->csv_1->splice( 1, 2 );

        $this->assertSame( 1, $this->csv_1->getRowCount() );

        $this->assertSame( [
            ['col_1','col_2'],
        ], $this->csv_1->toArray() );

        $this->assertFalse( isset($this->csv_1[2]) );
    }


    public function testDeleteLastThreeRows()
    {
        $this->csv_1->splice( 1, 3 );

        $this->assertSame( 1, $this->csv_1->getRowCount() );

        $this->assertSame( [
            ['col_1','col_2'],
        ], $this->csv_1->toArray() );

        $this->assertFalse( isset($this->csv_1[2]) );
    }



    public function testReplaceFirstRow()
    {
        $this->csv_1->splice( 0, 1, [['foo','bar']] );

        $this->assertSame( 3, $this->csv_1->getRowCount() );

        $this->assertSame( [
            ['foo','bar'],
            ['1','2'],
            ['3','4'],
        ], $this->csv_1->toArray() );

        $this->assertFalse( isset($this->csv_1[3]) );
    }



    public function testReplaceLastRow()
    {
        $this->csv_1->splice( 2, 1, [['foo','bar'], ['woo','war']] );

        $this->assertSame( 4, $this->csv_1->getRowCount() );

        $this->assertSame( [
            ['col_1','col_2'],
            ['1','2'],
            ['foo','bar'],
            ['woo','war'],
        ], $this->csv_1->toArray() );
    }

}