<?php
Namespace dgifford\CSV\Tests;



use dgifford\CSV\CSV;



require __DIR__ . '/../vendor/autoload.php';



class EditingTest extends TestCase
{
    public function setUp():void
    {
        parent::setUp();

        $this->csv_1 = new CSV( $this->csv_1_path );

        $this->inserted_result = [
            ['col_1','col_2'],
            ['col_1','col_2'],
            ['5','6'],
            ['7','8'],
            ['1','2'],
            ['3','4'],
        ];
    }



    public function testClear()
    {
        $this->assertSame( 3, $this->csv_1->getRowCount() );

        $this->csv_1->clear();

        $this->assertSame( 0, $this->csv_1->getRowCount() );
    }



    public function testReplaceWithFile()
    {
        $this->csv_1->replace( $this->csv_2_path );

        $this->assertSame( $this->csv_2_array, $this->csv_1->toArray() );
    }



    public function testReplaceWithInvalidPath()
    {
        $this->expectException( \InvalidArgumentException::class );

        $this->csv_1->replace( '/foo/bar.csv' );
    }



    public function testReplaceWithInvalidFile()
    {
        $this->expectException( \InvalidArgumentException::class );

        $this->csv_1->replace( $this->invalid_file_path );
    }


    public function testReplaceWithArray()
    {
        $this->csv_1->replace([
            [1,2,3],
            [4,5,6],
        ]);

        $this->assertSame( 2, $this->csv_1->getRowCount() );

        $this->assertSame( ['1','2','3'], $this->csv_1[0] );
    }



    public function testAppendFile()
    {
        $this->csv_1->append( $this->csv_2_path );

        $this->assertSame( array_merge( $this->csv_1_array, $this->csv_2_array ), $this->csv_1->toArray() );
    }




    public function testAppendWithInvalidPath()
    {
        $this->expectException( \InvalidArgumentException::class );

        $this->csv_1->append( '/foo/bar.csv' );
    }



    public function testAppendWithInvalidFile()
    {
        $this->expectException( \InvalidArgumentException::class );

        $this->csv_1->append( $this->invalid_file_path );
    }



    public function testAppendArray()
    {
        $this->csv_1->append([
            [1,2,3],
            [4,5,6],
        ]);

        $this->assertSame( 5, $this->csv_1->getRowCount() );

        $this->assertSame( ['1','2','3'], $this->csv_1[3] );
    }


    public function testPrependFile()
    {
        $this->csv_1->prepend( $this->csv_2_path );

        $this->assertSame( array_merge( $this->csv_2_array, $this->csv_1_array ), $this->csv_1->toArray() );
    }




    public function testPrependWithInvalidPath()
    {
        $this->expectException( \InvalidArgumentException::class );

        $this->csv_1->prepend( '/foo/bar.csv' );
    }



    public function testPrependWithInvalidFile()
    {
        $this->expectException( \InvalidArgumentException::class );

        $this->csv_1->prepend( $this->invalid_file_path );
    }



    public function testPrependArray()
    {
        $this->csv_1->prepend([
            [1,2,3],
            [4,5,6],
        ]);

        $this->assertSame( 5, $this->csv_1->getRowCount() );

        $this->assertSame( ['1','2','3'], $this->csv_1[0] );
    }



    public function testInsertFile()
    {
        $this->csv_1->insert( 1, $this->csv_2_path );

        $this->assertSame( 6, $this->csv_1->getRowCount() );

        $this->assertSame( $this->inserted_result, $this->csv_1->toArray() );
    }




    public function testInsertWithInvalidPath()
    {
        $this->expectException( \InvalidArgumentException::class );

        $this->csv_1->insert( 0, '/foo/bar.csv' );
    }



    public function testInsertWithInvalidFile()
    {
        $this->expectException( \InvalidArgumentException::class );

        $this->csv_1->insert( 0, $this->invalid_file_path );
    }



    public function testInsertArray()
    {
        $this->csv_1->insert( 1, $this->csv_2_array);

        $this->assertSame( 6, $this->csv_1->getRowCount() );

        $this->assertSame( $this->inserted_result, $this->csv_1->toArray() );
    }



    public function testDeleteRow()
    {
        unset($this->csv_1[0]);

        $this->assertSame( 2, $this->csv_1->getRowCount() );

        $this->assertSame( ['1','2'], $this->csv_1[0] );

        $this->csv_1->deleteRow( 0 );

        $this->assertSame( 1, $this->csv_1->getRowCount() );

        $this->assertSame( ['3','4'], $this->csv_1[0] );
    }



    public function testAppendRow()
    {
        $this->csv_1->appendRow( ['5','6'] );

        $this->assertSame( 4, $this->csv_1->getRowCount() );

        $this->assertSame( ['5','6'], $this->csv_1[3] );
    }



    public function testInsertRow()
    {
        $this->csv_1->insertRow( 0, ['5','6'] );

        $this->assertSame( 4, $this->csv_1->getRowCount() );

        $this->assertSame( ['3','4'], $this->csv_1[3] );
    }



    public function testReplaceRow()
    {
        $this->csv_1->replaceRow( 1, ['5','6'] );

        $this->assertSame( 3, $this->csv_1->getRowCount() );

        $this->assertSame( ['5','6'], $this->csv_1[1] );
    }



}