<?php
Namespace dgifford\CSV\Tests;



use dgifford\CSV\CSV;



require __DIR__ . '/../vendor/autoload.php';



class OutputTest extends TestCase
{
    public function setUp():void
    {
        parent::setUp();

        $this->csv = new CSV( $this->csv_1_path );

        $this->csv_2 = new CSV( $this->csv_2_path );

        $this->csv_1_array_with_header = [
            ['col_1','col_2'],
            ['col_1' => '1','col_2' => '2'],
            ['col_1' => '3','col_2' => '4'],
        ];
    }



	public function testRowCount()
	{
        $this->assertSame( 3, $this->csv->getRowCount() );
    }



	public function testColumnCount()
	{
        $this->assertSame( 2, $this->csv->getColumnCount() );
    }


    public function testIssetReturnsFalseForFileWithTrailingLineEnding()

    {
        $this->assertTrue( isset($this->csv_2[0]) );

        $this->assertTrue( isset($this->csv_2[1]) );

        $this->assertTrue( isset($this->csv_2[2]) );

        $this->assertFalse( isset($this->csv_2[3]) );
    }



	public function testArrayAccessWithoutHeader()
	{
        $this->assertSame( $this->csv_1_array[0], $this->csv[0] );

        $this->assertSame( $this->csv_1_array[1], $this->csv[1] );

        $this->assertSame( $this->csv_1_array[2], $this->csv[2] );
    }



	public function testArrayAccessWithHeader()
	{
        $this->csv->setHasHeader();

        $this->assertSame( $this->csv_1_array_with_header[0], $this->csv[0] );

        $this->assertSame( $this->csv_1_array_with_header[1], $this->csv[1] );

        $this->assertSame( $this->csv_1_array_with_header[2], $this->csv[2] );
    }



	public function testIteratorWithoutHeader()
	{
        foreach( $this->csv as $i => $row )
        {
            $this->assertSame( $this->csv_1_array[$i], $row );
        }

        $this->assertSame( $i, $this->csv->getRowCount() - 1 );
    }



	public function testIterateFileWithTrailingLineEnding()
	{
        foreach( $this->csv_2 as $i => $row )
        {
            $this->assertSame( $this->csv_2_array[$i], $row );
        }

        $this->assertSame( $i + 1, $this->csv->getRowCount() );
    }


	public function testIteratorWithHeader()
	{
        $this->csv->setHasHeader();

        foreach( $this->csv as $i => $row )
        {
            $this->assertSame( $this->csv_1_array_with_header[$i], $row );
        }

        $this->assertSame( $i + 1, $this->csv->getRowCount() );
    }



    public function testGetHeader()
    {
        $this->assertNull( $this->csv->getHeader() );

        $this->csv->setHasHeader();

        $this->assertSame( ['col_1','col_2'], $this->csv->getHeader() );
    }



	public function testCsvToString()
	{
        $str = file_get_contents($this->csv_1_path);

        $this->assertSame( $str, (string) $this->csv );

        $this->assertSame( $str, $this->csv->toString() );
    }



	public function testCsvToArray()
	{
        $this->assertSame( $this->csv_1_array, $this->csv->toArray() );
    }



    public function testSaveFile()
    {
        $this->deleteFile( $this->temp_csv_path );

        $this->csv->save( $this->temp_csv_path );

        $this->assertFileExists( $this->temp_csv_path );

        $csv_copy = new CSV( $this->temp_csv_path );

        $this->assertSame( $csv_copy->toString(), $this->csv->toString() );
    }

}