<?php /** @noinspection ALL */

Namespace dgifford\CSV;



class CSV implements CSVInterface
{
	/**
	 * Column delimiter character
	 * @var string
	 */
	protected string $delimiter = ',';



	/**
	 * String enclosure
	 * @var string
	 */
	protected string $enclosure = '"';



	/**
	 * Excape character sequence
	 * @var string
	 */
	protected string $escape_char = "\\";



	/**
	 * Max line length
	 * @var integer
	 */
	protected int $length = 0;



	/**
	 * Line ending character
	 * @var string
	 */
	protected string $line_ending = "\n";



	/**
	 * Whether the CSV has a header row
	 * @var boolean
	 */
	protected bool $has_header = false;



	/**
	 * Valid CSV mime types
	 * @var array
     */
	protected static array $csv_mime_types = ['text/csv' , 'text/plain'];



    /**
     * Temporary file handle.
     *
     * @var resource
     */
    protected $temp_file_handle;


    /**
     * Temporary file in memory limit.
     *
     * 10 Mb (1024 * 1024 * 10)
     * @var int
     */
    protected int $temp_file_memory_limit = 10485760;



    /**
     * Iterator position.
     * @var int
     */
    protected int $position = 0;


    /**
     * Create a CSV object with a path to a CSV or an array.
     *
     * @param null $data
     * @throws \Exception
     */
	public function __construct( $data = null )
	{
        $this->replace( $data );
	}


    /**
     * Instantiate and return object in one call
     *
     * @param null $data
     * @return CSV
     * @throws \Exception
     */
    public static function make( $data = null ): CSV
    {
        return new CSV( $data );
    }



    /**
     * Clear data
     *
     * @return CSV
     */
    public function clear(): CSV
    {
        $this->temp_file_handle = $this->getTempHandle();

        if( $this->temp_file_handle === false )
        {
            throw new \InvalidArgumentException('Cannot open temporary file handle.');
        }

        return $this;
    }



    /**
     * Set the CSV data. Data can be a path to a CSV,
     * a CSV object, a multidimensional array or null.
     *
     * @param null $data
     * @return CSV
     * @throws \Exception
     */
    public function replace($data = null ): CSV
    {
        $this->clear();

        if( !is_null($data) )
        {
            $this->append( $data );
        }

        return $this;
    }



    /**
     * Add data to the start of the CSV. Data can be a path to a CSV,
     * a CSV object, a multidimensional array or null.
     *
     * @param $data
     * @return $this        Chainable
     * @throws \Exception
     */
	public function append( $data ): CSV
	{
        return $this->splice( -1, 0, $data );
	}



    /**
     * Add data to the end of the CSV. Data can be a path to a CSV,
     * a CSV object, a multidimensional array or null.
     *
     * @param $data
     * @return $this        Chainable
     * @throws \Exception
     */
	public function prepend( $data ): CSV
	{
        return $this->splice( 0, 0, $data );
	}



    /**
     * Insert data at the given index in the CSV. Data can be a
     * path to a CSV, a CSV object, a multidimensional array or null.
     *
     * @param int $index
     * @param $data
     * @return $this
     * @throws \Exception
     */
	public function insert( int $index, $data ): CSV
	{
        return $this->splice( $index, 0, $data );
	}



    /**
     * Splice data into the CSV. Data can be a path to a CSV,
     * a CSV object, a multidimensional array or null.
     *
     * The data replaces $length rows starting from $offset row.
     *
     * $offset can have a special value of -1 to append the data.
     *
     * @param int $offset
     * @param int $length
     * @param $replacement
     * @return $this
     * @throws \Exception
     */
    public function splice( int $offset, int $length, $replacement = null )
    {
        // Check for exceptions
        if( is_string( $replacement ) and !self::isValidFile( $replacement ) )
        {
            throw new \InvalidArgumentException('Invalid CSV File: ' . $replacement );
        }

        if( is_array( $replacement ) and (!self::isMultiDimensionalArray( $replacement, true ) or empty($replacement)) )
        {
            throw new \InvalidArgumentException("Data is not a multidimensional array.");
        }

        if( is_object( $replacement ) and !$replacement instanceof CSVInterface )
        {
            throw new \InvalidArgumentException("Data is not a valid CSV object.");
        }

        if( $offset < -1 )
        {
            throw new \InvalidArgumentException("Offset cannot be less than -1.");
        }

        if( $length < 0 )
        {
            throw new \InvalidArgumentException("Length must be more than or equal to 0");
        }

        // Move to start of temp file, copy handle and clear temp file
        $this->seekStart();
        $old_handle = $this->temp_file_handle;
        $this->clear();

        // Copy rows up to offset into temp file
        $count = 0;
        while(
            ($count < $offset or $offset == -1)
            and
            ($row = $this->getCSVRow($old_handle)) !== false
        )
        {
            $this->putCSVRow( $this->temp_file_handle, $row );

            $count++;
        }

        // Move past unwanted lines in old handle
        while( $length > 0 and $this->getCSVRow($old_handle) !== false )
        {
            $length--;
        }

        // Add a line ending if required
        if( $offset > 0 )
        {
            $this->addTrailingLineEnding();
        }

        // Add replacement data
        if( is_string( $replacement ) )
        {
            $handle = $this->getFileHandle( $replacement );

            stream_copy_to_stream( $handle, $this->temp_file_handle );

            fclose( $handle );
        }
        elseif( is_array($replacement) or $replacement instanceof CSV )
        {
            foreach( $replacement as $row )
            {
                //var_export($row);
                $this->putCSVRow( $this->temp_file_handle, $row );
            }
        }

        // Copy rest of old file
        stream_copy_to_stream( $old_handle, $this->temp_file_handle );

        fclose( $old_handle );

        $this->removeTrailingLineEnding();

        return $this;
    }



    /**
     * Adds a trailing line ending to the temp file stream
     *
     * @return void
     * @throws \Exception
     */
    protected function addTrailingLineEnding(): void
    {
        $current = ftell( $this->temp_file_handle );

        $this->seekEnd();

        fwrite( $this->temp_file_handle, $this->getLineEnding() );

        // Reset position
        fseek( $this->temp_file_handle, $current, SEEK_SET);
    }



    /**
     * Remove a trailing line ending from the CSV.
     *
     * @return void
     * @throws \Exception
     */
    protected function removeTrailingLineEnding(): void
    {
        if( $this->getTempFileSize() > 0 )
        {
            $this->seekEnd( $this->getLineEndingLength() * -1 );

            $str = fread( $this->temp_file_handle, $this->getLineEndingLength());

            if( $str == $this->getLineEnding() )
            {
                $new_length = $this->getTempFileSize() - $this->getLineEndingLength();
                $result = ftruncate( $this->temp_file_handle, $new_length );
            }
        }
    }



	/**
	 * Write a CSV formatted row to a file.
	 *
	 * @param  resource $handle [description]
	 * @param  array $row    [description]
	 * @return false|int
	 */
	protected function putCSVRow($handle, array $row )
	{
        if( !is_resource($handle) )
        {
            throw new \InvalidArgumentException('Handle is not a valid resource' );
        }

		return fputcsv(
            $handle,
            $row,
            $this->getDelimiter(),
            $this->getEnclosure(),
            $this->getEscapeChar()
        );
	}



    /**
     * Return the next row array from a CSV file.
     *
     * @param $handle
     * @return array|false
     */
    protected function getCSVRow( $handle )
    {
        if( !is_resource($handle) )
        {
            throw new \InvalidArgumentException('Handle is not a valid resource' );
        }

        $row = fgetcsv(
            $handle,
            $this->getLength(),
            $this->getDelimiter(),
            $this->getEnclosure(),
            $this->getEscapeChar()
        );

        if( is_null($row) )
        {
            throw new \InvalidArgumentException('Handle is invalid' );
        }

        return $row;
    }



	/**
	 * Get a file handle for the given path.
	 * Throw exceptions if the path is a directory or not readable
	 * or not writable
	 *
	 * @return resource
	 */
	protected function getFileHandle( string $path, string $mode = 'r' )
	{
		if( is_dir( $path ) )
		{
			throw new \InvalidArgumentException('Path is a directory: ' . $path );
		}

		if( ($handle = fopen( $path, $mode)) === false )
		{
			throw new \InvalidArgumentException("Could not open CSV in mode '{$mode}': {$path}" );
		}

		return $handle;
	}


    /**
     * Get the CSV file size.
     *
     * @return mixed
     */
    protected function getTempFileSize()
    {
        $stat = fstat( $this->temp_file_handle );

        return $stat['size'];
    }



	/////////////////////////////////////
	// Output
	/////////////////////////////////////



    /**
     * Return the data as a string
     * @return string
     */
	public function toString(): string
	{
        $str = '';

        if( $this->getTempFileSize() > 0 )
        {
            $current = ftell( $this->temp_file_handle );

            $str = stream_get_contents( $this->temp_file_handle, null, 0);

            // Reset file position
            fseek( $this->temp_file_handle, $current, SEEK_SET);
        }

        return $str;
	}



    /**
     * @return string
     */
	public function __toString(): string
	{
		return $this->toString();
	}



    /**
     * Return all data as an array.
     *
     * @return array
     */
	public function toArray(): array
	{
        $arr = [];

        $this->seekStart();

        while( ($row = $this->getCSVRow($this->temp_file_handle)) !== false )
        {
            $arr[] = $row;
        }
        return $arr;
	}


    /**
     * Save CSV to a path.
     * @param string $path
     * @return void
     */
	public function save( string $path ): void
	{
        file_put_contents( $path, $this->toString() );
	}


    /**
     * Seek a number of bytes from start of temp file.
     *
     * @param int $offset
     * @return void
     * @throws \Exception
     */
    protected function seekStart( int $offset = 0 ): void
    {
        $this->seek( $offset );
    }


    /**
     * Forward temp file pointer to end.
     * @param int $offset
     * @return void
     * @throws \Exception
     */
    protected function seekEnd( int $offset = 0 ): void
    {
        if( $offset > 0 )
        {
            throw new \Exception("Error attempting to seek $offset bytes before end of temp file. Offset must be negative.");
        }

        $this->seek( $offset, SEEK_END);
    }



    /**
     * Seek a point in temp file.
     *
     * @param int $offset   Number of bytes.
     * @param int $type     SEEK_SET or SEEK_CUR or SEEK_END
     * @return void
     * @throws \Exception
     */
    protected function seek( int $offset = 0, int $type = SEEK_SET )
    {
        fseek( $this->temp_file_handle, $offset, $type);
    }



	/////////////////////////////////////
	// Boolean tests
	/////////////////////////////////////



	/**
	 * Return true if a row exists.
	 *
	 * @param  int    $index
	 * @return mixed
	 */
	public function rowExists( int $index ): bool
	{
        $row = $this->getRow( $index );

        if( is_array($row) )
        {
            return true;
        }

        return false;
	}



	/**
	 * Boolean check if CSV has header replace.
	 *
	 * @return boolean
	 */
	public function hasHeader(): bool
	{
		return $this->has_header;
	}


    /**
     * Check if a csv file path is valid.
     *
     * @param string $path
     * @return bool
     */
	public static function isValidFile( string $path ): bool
	{
		if(
			!is_file( $path )
			or !is_readable( $path )
			or !in_array( strtolower( mime_content_type( $path ) ), self::$csv_mime_types )
		)
		{
			throw new \InvalidArgumentException('Invalid CSV file: ' . $path );
		}

		return true;
	}



	/**
	 * Boolean test if an array is multidimensional (contains other arrays).
	 * When strict is false, true is returned as soon as an internal array
	 * is found. When strict is true, all internal elements must be arrays
	 *
	 * @param  array   $arr
	 * @param  bool    $strict
	 * @return boolean
	 */
	public static function isMultiDimensionalArray( array $arr, bool $strict = false ): bool
	{
		if( !is_array($arr) )
		{
			return false;
		}

		foreach( $arr as $value )
		{
			if( !$strict and is_array($value) )
			{
				return true;
			}
			elseif( $strict and !is_array($value) )
			{
				return false;
			}
		}

		return $strict;
	}



	/**
	 * Internal method to check parameters are 1 char
	 * @param  string  $char
	 * @param  string  $message
	 * @return boolean
	 */
	protected function isOneChar( string $char, string $message ): bool
    {
		if( strlen( $char ) != 1 )
		{
			throw new \InvalidArgumentException( $message . ' can only be 1 character long');
		}

		return true;
	}



	/////////////////////////////////////
	// Setters for parameters
	/////////////////////////////////////



	/**
	 * Call the setter for each property in the given
	 * config array, if it exists.
	 *
	 * @param array $config
	 * @return $this Chainable
	 */
	public function setConfig( array $config = [] ): CSV
	{
		foreach( $config as $name => $value )
		{
			$method_a = array_merge(['replace'], array_map( 'ucfirst', explode('_', $name) ));

			$method = implode('', $method_a);

			if( is_callable([$this, $method]) )
			{
				$this->$method( $value );
			}
		}

		return $this;
	}



	/**
	 * Set the column delimiter
	 * @param string $delimiter
	 * @return $this Chainable
	 */
	public function setDelimiter( string $delimiter ): CSV
	{
		if( $this->isOneChar( $delimiter, 'Delimiter' ) )
		{
			$this->delimiter = $delimiter;
		}

		return $this;
	}



	/**
	 * Set the destination string enclosure
	 * @param string $enclosure
	 * @return $this Chainable
	 */
	public function setEnclosure( string $enclosure ): CSV
	{
		if( $this->isOneChar( $enclosure, 'Enclosure' ) )
		{
			$this->enclosure = $enclosure;
		}

		return $this;
	}



	/**
	 * Set the destination escape character
	 * @param string $escape_char
	 * @return $this Chainable
	 */
	public function setEscapeChar( string $escape_char ): CSV
	{
		if( $this->isOneChar( $escape_char, 'Escape character' ) )
		{
			$this->escape_char = $escape_char;
		}

		return $this;
	}



	/**
	 * Set the max line length
	 * @param string $length
	 * @return $this Chainable
	 */
	public function setLength( int $length ): CSV
	{
		$this->length = $length;

		return $this;
	}



	/**
	 * Set the line ending string
	 * @param string $line_ending
	 * @return $this Chainable
	 */
	public function setLineEnding( string $line_ending ): CSV
	{
		$this->line_ending = $line_ending;

		return $this;
	}


    /**
     * Set the header. If the CSV has a header it is replaced
     * otherwise it is inserted and the CSV is set to have a
     * header.
     *
     * @param array $header
     * @return void
     * @throws \Exception
     */
    public function setHeader( array $header )
    {
        if( $this->hasHeader() )
        {
            $this->replaceRow( 0, $header );
        }
        else
        {
            $this->setHasHeader();
            $this->insertRow( 0, $header );
        }
    }



	/**
	 * Specify that the CSV has a header
	 * @param boolean $has_header
	 * @return $this Chainable
	 */
	public function setHasHeader( bool $has_header = true ): CSV
	{
		$this->has_header = $has_header;

		return $this;
	}


    /**
     * Add a single row to the end of the CSV.
     *
     * @param array $row
     * @return $this
     * @throws \Exception
     */
    public function appendRow( array $row ): CSV
    {
        $this->splice( -1, 0, [$row] );

        return $this;
    }



    /**
     * Add a single row to the start of the CSV.
     *
     * @param array $row
     * @return $this
     * @throws \Exception
     */
    public function prependRow( array $row ): CSV
    {
        $this->splice( 0, 0, [$row] );

        return $this;
    }


    /**
     * Insert a row at a given index.
     *
     * @param int $index
     * @param array $new_row
     * @return $this
     * @throws \Exception
     */
    public function insertRow( int $index, array $row ): CSV
    {
        $this->splice( $index, 0, [$row] );

        return $this;
    }



    /**
     * Delete a row.
     *
     * @param int $index
     * @return $this
     * @throws \Exception
     */
    public function deleteRow( int $index ): CSV
    {
        $this->splice( $index, 1 );

        return $this;
    }



    /**
     * Set the row at the given index.
     *
     * @param int $index
     * @param array $new_row
     * @return $this
     * @throws \Exception
     */
    public function replaceRow( int $index, array $row ): CSV
    {
        $this->splice( $index, 1, [$row] );

        return $this;
    }



    /**
     * Return a temp in memory file handle
     * @param string $mode
     * @return false|resource
     */
    protected function getTempHandle( string $mode = 'w' )
    {
        return fopen('php://temp/maxmemory:' . $this->temp_file_memory_limit, $mode );
    }




	/////////////////////////////////////
	// Getters
	/////////////////////////////////////



	/**
	 * Use PHPs fputcsv to create a CSV row string
	 * in memory.
	 *
	 * @param  array  $row
	 * @return string
	 */
	protected function getCSVRowString( array $row )
	{
		$handle = fopen( 'php://memory', 'r+' );

		if( fputcsv( $handle, $row, $this->delimiter, $this->enclosure, $this->escape_char ) == false )
		{
			throw new \Exception('Could not create row string');
		}

		rewind($handle);

		$str = stream_get_contents($handle);

		fclose($handle);

		return $str;
	}




    /**
     * Returns the row at the given index as an array.
     * If the CSV has a header, the array will be indexed
     * by column names.
     *
     * Returns null if the row cannot be found.
     *
     * @param int $index
     * @return array|null
     */
	public function getRow( int $index ): ?array
	{
        $this->seekStart();

        $count = 0;

        while( ($row = $this->getCSVRow($this->temp_file_handle)) !== false )
        {
            if( $index == $count )
            {
                if( $this->hasHeader() and $index > 0 )
                {
                    return array_combine( $this->getHeader(), $row );
                }
                else
                {
                    return $row;
                }
            }

            $count++;
        }

		return null;
	}



	/**
	 * Return the header as an array of column names
	 * or null if the CSV has no header.
	 *
	 * @return array|null
	 */
	public function getHeader(): ?array
	{
		if( $this->hasHeader() )
		{
			return $this->getRow(0);
		}

		return null;
	}




	/**
	 * Get the column delimiter
	 * @return string
	 */
	public function getDelimiter(): string
	{
		return $this->delimiter;
	}



	/**
	 * Get the destination string enclosure
	 * @return string
	 */
	public function getEnclosure(): string
	{
		return $this->enclosure;
	}



	/**
	 * Get the destination escape character
	 * @return string
	 */
	public function getEscapeChar(): string
	{
		return $this->escape_char;
	}



	/**
	 * Get the CSV line length
	 * @return string
	 */
	public function getLength(): int
	{
		return $this->length;
	}



	/**
	 * Get the destination escape character
	 * @return string
	 */
	public function getLineEnding(): string
	{
		return $this->line_ending;
	}


    /**
     * Return multibyte length of line ending.
     * @return int
     */
	public function getLineEndingLength(): int
	{
		return mb_strlen($this->getLineEnding());
	}



    /**
     * Return the number of rows.
     * Alias of count().
     *
     * @return int
     */
    public function getRowCount(): int
    {
        return $this->count();
    }



    /**
     * Return number of columns based on first row.
     *
     * @return int
     */
    public function getColumnCount(): int
    {
        return count($this[0]);
    }



	/////////////////////////////////////
	// Implement PHP interfaces
	/////////////////////////////////////



	/**
	 * Return number of rows.
     * Implements Countable interface.
     *
	 * @return integer
	 */
	public function count(): int
	{
        $count = 0;

        $this->seekStart();

        while( $this->getCSVRow($this->temp_file_handle) !== false )
        {
            $count++;
        }

        return $count;
    }



    /**
     * Rewind iterator to first element.
     *
     * @return void
     */
	public function rewind(): void
    {
		$this->position = 0;
	}


    /**
     * Return row at current position in iterator.
     *
     * @return array|null
     */
	public function current()
	{
		return $this->getRow( $this->position );
	}


    /**
     * Key of current element in iterator.
     *
     * @return int
     */
	public function key()
	{
		return $this->position;
	}


    /**
     * Increment iterator position.
     *
     * @return void
     */
	public function next()
	{
		$this->position++;
	}


    /**
     * Check if iterator position is valid.
     *
     * @return bool
     */
	public function valid(): bool
    {
		return $this->position < $this->count();
	}


    /**
     *  Set row at array index
     *
     * @param $offset
     * @param $value
     * @return void
     */
	public function offsetSet($offset, $value): void
    {
		$this->replaceRow( $offset, $value );
	}


    /**
     * Boolean check if index exists
     * @param $offset
     * @return bool
     */
	public function offsetExists($offset): bool
    {
		return $this->rowExists($offset);
	}


    /**
     * Unset/ delete a row.
     *
     * @param $offset
     * @return void
     */
	public function offsetUnset($offset)
	{
		$this->deleteRow( $offset );
	}


    /**
     * Return row at array offset.
     *
     * @param $offset
     * @return array|null
     */
	public function offsetGet($offset)
	{
		return $this->getRow( $offset );
	}
}