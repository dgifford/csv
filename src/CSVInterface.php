<?php
Namespace dgifford\CSV;



interface CSVInterface extends \Stringable, \Iterator, \ArrayAccess, \Countable
{
    /**
     * Instantiate and return object in one call
     *
     * @param null $data
     * @return CSVInterface
     */
    public static function make( $data = null ): CSVInterface;



    /**
     * Set the content of the CSV from a file or array.
     *
     * @param null $data
     * @return CSVInterface
     */
    public function replace($data = null ): CSVInterface;



    /**
     * Clear data
     * @return CSVInterface
     */
    public function clear(): CSVInterface;



    /**
     * Append data from a file or multidimensional array.
     * @param $data
     * @return CSVInterface
     */
	public function append( $data ): CSVInterface;



    /**
     * Insert data at a given index. Use -1 to append to end of file.
     *
     * @param int $index
     * @param $data
     * @return $this
     * @throws \Exception
     */
    public function insert( int $index, $data ): CSVInterface;


    /**
     * Append a row of data.
     *
     * @param array $row
     * @return CSVInterface
     */
	public function appendRow( array $row ): CSVInterface;



    /**
     * Delete an existing row.
     *
     * @param int $index
     * @return CSVInterface
     */
    public function deleteRow( int $index ): CSVInterface;



    /**
     * Return the data as a string
     * @return string
     */
	public function toString(): string;


    /**
     * Return the data as an array
     * @return array
     */
	public function toArray(): array;


    /**
     * Save CSV to a path.
     * @param string $path
     * @return void
     */
	public function save( string $path ): void;



	/**
	 * Return true if a row exists.
	 * 
	 * @param  int    $index
	 * @return mixed
	 */
	public function rowExists( int $index ): bool;



	/**
	 * Boolean check if CSV has header replace.
	 * 
	 * @return boolean
	 */
	public function hasHeader(): bool;



    /**
     * Check if a csv file path is valid.
     * @param string $path
     * @return bool
     */
	public static function isValidFile( string $path ): bool;



	/**
	 * Boolean test if a array is multi-dimensional (contains other arrays).
	 * When strict is false, true is returned as soon as an internal array
	 * is found. When strict is true, all internal elements must be arrays
	 * 
	 * @param  array   $arr
	 * @param  bool    $strict
	 * @return boolean
	 */
    public static function isMultiDimensionalArray( array $arr, bool $strict = false ): bool;



	/**
	 * Call the setter for each property in the given
	 * config array, if it exists.
	 * 
	 * @param array $config
	 * @return $this Chainable
	 */
	public function setConfig( array $config = [] ): CSVInterface;



	/**
	 * Set the column delimiter
	 * @param string $delimiter
	 * @return $this Chainable
	 */
	public function setDelimiter( string $delimiter ): CSVInterface;



	/**
	 * Set the destination string enclosure
	 * @param string $enclosure
	 * @return $this Chainable
	 */
	public function setEnclosure( string $enclosure ): CSVInterface;



	/**
	 * Set the destination escape character
	 * @param string $escape_char
	 * @return $this Chainable
	 */
	public function setEscapeChar( string $escape_char ): CSVInterface;



	/**
	 * Set the max line length
	 * @param string $length
	 * @return $this Chainable
	 */
	public function setLength( int $length ): CSVInterface;



	/**
	 * Set the line ending string
	 * @param string $line_ending
	 * @return $this Chainable
	 */
	public function setLineEnding( string $line_ending ): CSVInterface;



	/**
	 * Specify that the CSV has a header
	 * @param boolean $has_header
	 * @return $this Chainable
	 */
	public function setHasHeader( bool $has_header = true ): CSVInterface;



	/**
	 * Returns the row at the given index as an array.
	 * If the CSV has a header, the array will be indexed
	 * by column names.
	 * 
	 * @param  int    $index 
	 * @return array
	 */
	public function getRow( int $index ): ?array;



	/**
	 * Return the header as an array of column names
	 * or null if the CSV has no header.
	 *
	 * @return array|null
	 */
	public function getHeader(): ?array;




	/**
	 * Get the column delimiter
	 * @return string
	 */
	public function getDelimiter(): string;



	/**
	 * Get the destination string enclosure
	 * @return string
	 */
	public function getEnclosure(): string;



	/**
	 * Get the destination escape character
	 * @return string
	 */
	public function getEscapeChar(): string;



	/**
	 * Get the CSV line length
	 * @return string
	 */
	public function getLength(): int;



	/**
	 * Get the destination escape character
	 * @return string
	 */
	public function getLineEnding(): string;



    /**
     * Return the number of rows.
     * Alias of count().
     *
     * @return int
     */
    public function getRowCount(): int;



    /**
     * Return number of columns based on first row.
     *
     * @return int
     */
    public function getColumnCount(): int;
}