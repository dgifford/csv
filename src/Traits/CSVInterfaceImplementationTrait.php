<?php
Namespace dgifford\CSV\Traits;


/**
 *  Methods to implement interfaces:
 *  	Iterator
 *  	ArrayAccess
 *  	Countable
 */


trait CSVInterfaceImplementationTrait
{
	/**
	 * Return the number of rows
	 * @return integer
	 */
	public function count(): int
	{
		return count( $this->index );
	}



	public function rewind()
	{
		$this->position = 0;
	}	

	
	
	public function current()
	{
		return $this->getRow( $this->position );
	}



	public function key()
	{
		return $this->position;
	}



	public function next()
	{
		$this->position++;
	}



	public function valid()
	{
		return $this->position < ($this->count() - 1);
	}	



	public function offsetSet($offset, $value)
	{
		$this->setRow( $offset, $value );
	}



	public function offsetExists($offset)
	{
		return isset($this->index[$offset]);
	}



	public function offsetUnset($offset)
	{
		$this->deleteRow( $offset );
	}



	public function offsetGet($offset)
	{
		return $this->getRow( $offset );
	}
}