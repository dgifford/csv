<?php
Namespace dgifford\CSV\Traits;



use dgifford\CSV\CSV;


trait CSVConfigTrait
{
	/**
	 * Column delimiter character
	 * @var string
	 */
	protected $delimiter = ',';



	/**
	 * String enclosure
	 * @var string
	 */
	protected $enclosure = '"';



	/**
	 * Excape character sequence
	 * @var string
	 */
	protected $escape_char = "\\";



	/**
	 * max line length
	 * @var integer
	 */
	protected $length = 0;



	/**
	 * Line ending character
	 * @var string
	 */
	protected $line_ending = "\n";



	/**
	 * Whether the CSV has a header row
	 * @var boolean
	 */
	protected $has_header = false;



	/**
	 * Block size used to index files
	 * @var integer
	 */
	protected $byte_block_size = 8192;



	/**
	 * Valid CSV mime types
	 * @var integer
	 */
	protected $csv_mime_types = ['text/csv' , 'text/plain'];



	/**
	 * Path to source CSV file
	 * @var string
	 */
	protected $source_path;





    /**
     * Path to file
     * @var string
     */
    protected $path;



    /**
     * File resource
     * @var Resource
     */
    protected $handle;



    /**
	 * Internal method to check parameters are 1 char
	 * @param  string  $char
	 * @param  string  $message
	 * @return boolean
	 */
	protected function isOneChar( string $char, string $message )
	{
		if( strlen( $char ) != 1 )
		{
			throw new \InvalidArgumentException( $message . ' can only be 1 character long');
		}

		return true;
	}



	/**
	 * Boolean check for an array or SplFixedArray
	 * @param  mixed  $arr
	 * @return boolean
	 */
	protected function isArray( $arr ):bool
	{
		return
			is_array($arr)
			or
			$arr instanceof \SplFixedArray;
	}



	/**
	 * Boolean test if a array is multi-dimensional (contains other arrays).
	 * When strict is false, true is returned as soon as an internal array
	 * is found. When strict is true, all internal elements must be arrays.
	 *
	 * Accepts tradistional PHP arrays and \SplFixedArray
	 * 
	 * @param  array|\SplFixedArray		$arr
	 * @param  bool    					$strict
	 * @return boolean
	 */
	protected function isMultiDimensionalArray( $arr, bool $strict = false ): bool
	{
		if( !$this->isArray($arr) )
		{
			return false;
		}

		foreach( $arr as $value )
		{
			if( !$strict and $this->isArray($value) )
			{
				return true;
			}
			elseif( $strict and !$this->isArray($value) )
			{
				return false;
			}
		}


		return $strict;
	}



	/////////////////////////////////////
	// Setters for parameters
	/////////////////////////////////////



	/**
	 * Call the setter for each property in the given
	 * config array, if it exists.
	 * 
	 * @param array $config
	 * @return $this Chainable
	 */
	public function setConfig( array $config )
	{
		foreach( $config as $name => $value )
		{
			$method_a = array_merge(['replace'], array_map( 'ucfirst', explode('_', $name) ));

			$method = implode('', $method_a);

			if( is_callable([$this, $method]) )
			{
				$this->$method( $value );
			}
		}

		return $this;
	}



	/**
	 * Set source path, ensuring it is a valid CSV
	 * 
	 * @param array $source_path
	 */
	public function setSourcePath( string $source_path )
	{
		if( !$this->isValidCSVFile( $source_path ) )
		{
			throw new \InvalidArgumentException('Invalid CSV file: ' . $source_path );
		}

		$this->source_path = $source_path;

		return $this;
	}



	/**
	 * Set path, ensuring directory exists
	 * @param array $path
	 */
	public function setPath( string $path )
	{
		if( !$this->isValidPath( $path ) )
		{
			throw new \InvalidArgumentException('Invalid CSV path: ' . $path );
		}

		$this->path = $path;

		return $this;
	}



	/**
	 * Set Accepted CSV mime types
	 * @param array $csv_mime_types
	 */
	public function setCsvMimeTypes( array $csv_mime_types )
	{
		$this->csv_mime_types = $csv_mime_types;

		return $this;
	}



	/**
	 * Set the column delimiter
	 * @param string $delimiter
	 * @return $this Chainable
	 */
	public function setDelimiter( string $delimiter )
	{
		if( $this->isOneChar( $delimiter, 'Delimiter' ) )
		{
			$this->delimiter = $delimiter;
		}

		return $this;
	}



	/**
	 * Set the destination string enclosure
	 * @param string $enclosure
	 * @return $this Chainable
	 */
	public function setEnclosure( string $enclosure )
	{
		if( $this->isOneChar( $enclosure, 'Enclosure' ) )
		{
			$this->enclosure = $enclosure;
		}

		return $this;
	}



	/**
	 * Set the destination escape character
	 * @param string $escape_char
	 * @return $this Chainable
	 */
	public function setEscapeChar( string $escape_char )
	{
		if( $this->isOneChar( $enclosure, 'Escape character' ) )
		{
			$this->escape_char = $escape_char;
		}

		return $this;
	}



	/**
	 * Set the max line length
	 * @param string $length
	 * @return $this Chainable
	 */
	public function setLength( int $length )
	{
		$this->length = $length;

		return $this;
	}



	/**
	 * Set the line ending string
	 * @param string $line_ending
	 * @return $this Chainable
	 */
	public function setLineEnding( string $line_ending )
	{
		$this->line_ending = $line_ending;

		return $this;
	}



	/**
	 * Specify that the CSV has a header
	 * @param boolean $has_header
	 * @return $this Chainable
	 */
	public function setHasHeader( bool $has_header = true )
	{
		$this->has_header = $has_header;

		return $this;
	}



	/**
	 * Set the block size used for transfering
	 * blocks of data betweenen files.
	 * 
	 * @param int $size
	 * @return $this Chainable
	 */
	public function setByteBlockSize( int $size )
	{
		$this->byte_block_size = $size;

		return $this;
	}



	/**
	 * Set the chunk size used when writing files.
	 * 
	 * @param array $write_chunk_size
	 * @return $this Chainable
	 */
	public function setWriteChunkSize( int $write_chunk_size )
	{
		$this->write_chunk_size = $write_chunk_size;

		return $this;
	}



	/**
	 * Get the column delimiter
	 * @return string
	 */
	public function getDelimiter(): string
	{
		return $this->delimiter;
	}



	/**
	 * Get the destination string enclosure
	 * @return string
	 */
	public function getEnclosure(): string
	{
		return $this->enclosure;
	}



	/**
	 * Get the destination escape character
	 * @return string
	 */
	public function getEscapeChar(): string
	{
		return $this->escape_char;
	}



	/**
	 * Get the CSV line length
	 * @return string
	 */
	public function getLength(): int
	{
		return $this->length;
	}



	/**
	 * Get the destination escape character
	 * @return string
	 */
	public function getLineEnding(): string
	{
		return $this->line_ending;
	}
}