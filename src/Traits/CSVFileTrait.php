<?php
Namespace dgifford\CSV\Traits;



trait CSVFileTrait
{
	/**
	 * Check if a csv file path is valid.
	 * 
	 * @param string $path
	 * @return boolean
	 */
	protected function isValidCSVFile( string $path ): bool
	{
		return
			is_file( $path ) 
			and is_readable( $path )
			and in_array( strtolower( mime_content_type( $path ) ), $this->csv_mime_types );
	}



	/**
	 * Check if a path directory exists and is writeable
	 * @param  string
	 * @return boolean
	 */
	protected function isValidPath( string $path ): bool
	{
		return
			is_dir( dirname($path) ) 
			and is_writeable( dirname($path) );
	}



	/**
	 * Get a file handle for the given path.
	 * Throw exceptions if the path is a directory or not readable
	 * or not writable
	 * 
	 * @return resource
	 */
	protected function getFileHandle( string $path, string $mode = 'r' )
	{
		if( is_dir( $path ) )
		{
			throw new \InvalidArgumentException('Path is a directory: ' . $path );
		}

		if( ($handle = fopen( $path, $mode)) === false )
		{
			throw new \InvalidArgumentException("Could not open CSV in mode '{$mode}': {$path}" );
		}

		return $handle;
	}



	/**
	 * Close the object's file handle.
	 * 
	 * @return void
	 */
	public function close()
	{
		if( is_resource( $this->handle ) )
		{
			fclose( $this->handle );
		}
	}

}