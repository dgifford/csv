<?php
Namespace dgifford\CSV\Tests;



use dgifford\CSV\CSVArray;



require __DIR__ . '/../vendor/autoload.php';



function out( $message = '' )
{
	echo "{$message}\n";
}



function humanReadable($bytes, $decimals = 2)
{
	$sz = str_split('BKMGTP');

	$factor = floor((strlen($bytes) - 1) / 3);

	return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . $sz[$factor];
}



function get100MArray()
{
	return array_fill( 0, 10000, array_fill( 0, 954, '1234567890' ) );
}



$tests =
[
	'testCreateFrom100MbArray' => function()
	{
		$csv = new CSVArray( get100MArray() );
		out( 'Memory usage: ' . humanReadable(memory_get_usage()) );
	},

	'testPrepend100MbArray' => function()
	{
		$csv = new CSVArray( get100MArray() );
		$csv->prepend( get100MArray() );
		out( 'Memory usage: ' . humanReadable(memory_get_usage()) );
	},

	'testInsert100MbArray' => function()
	{
		$csv = new CSVArray( get100MArray() );
		$csv->insert( 5000, get100MArray() );
		out( 'Memory usage: ' . humanReadable(memory_get_usage()) );
	},

	'testAppend100MbArray' => function()
	{
		$csv = new CSVArray( get100MArray() );
		$csv->append( get100MArray() );
		out( 'Memory usage: ' . humanReadable(memory_get_usage()) );
	},

	'testAppend100MbArray' => function()
	{
		$csv = new CSVArray( get100MArray() );
		$csv->append( get100MArray() );
		$csv->append( get100MArray() );
		out( 'Memory usage: ' . humanReadable(memory_get_usage()) );
	},
];



foreach( $tests as $name => $callable )
{
	out();
	out( $name );
	out( '-----------------------------' );

	$start_time = microtime(true);
	$start_mem = memory_get_usage();
	
	call_user_func($callable);

	$end_time = microtime(true);
	$end_mem = memory_get_usage();

	out( 'Time: ' . ($end_time - $start_time) . 'ms' );
	out( 'Memory increase: ' . humanReadable($end_mem - $start_mem) );

}

